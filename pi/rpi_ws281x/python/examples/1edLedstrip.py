import time

from neopixel import *

# LED strip configuration:
LED_COUNT      = 60      # Number of LED pixels.
LED_PIN        = 12      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 30      # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)



def colorWipe(strip, color, wait_ms=0):
	#Wipe color across display a pixel at a time
	for i in range(strip.numPixels()):
		strip.setPixelColor(i, color)
		strip.show()
		time.sleep(wait_ms/1000.0)

def bunchWipe(strip, pin, inColor, wait_ms=0):
        #Wipe bunch of leds across strip
        bunchSize = 10
        if pin < bunchSize:
                strip.setPixelColor(pin, inColor)
        elif pin >= LED_COUNT-1:
                strip.setPixelColor(pin, inColor)
                for i in range(LED_COUNT-bunchSize-1, LED_COUNT):
                        strip.setPixelColor(i, Color(0,0,0))
                        #strip.show()
        else:
                strip.setPixelColor(pin, inColor)
                strip.setPixelColor(pin-bunchSize, Color(0,0,0))
        strip.show()


def singleWipe(strip, pin, inColor, wait_ms=0):
        #Wipe single led across strip
        if pin == 0:
                strip.setPixelColor(pin, inColor)
        else:
                strip.setPixelColor(pin, inColor)
                strip.setPixelColor(pin-1, Color(0,0,0))
        strip.show()

        

# Main program logic follows:
if __name__ == '__main__':
	# Create NeoPixel object with appropriate configuration.
	strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
	# Intialize the library (must be called once before other functions).
	strip.begin()

	print ('Press Ctrl-C to quit.')
	for i in range(strip.numPixels()):
                strip.setPixelColor(i, Color(0,0,0))
	#strip.setPixelColor(0, Color(100,100,100))
	strip.show()


	while True:
                """
		#-----Color wipe animations-----
		colorWipe(strip, Color(255, 0, 0))  # Red wipe
		colorWipe(strip, Color(0, 255, 0))  # Blue wipe
		colorWipe(strip, Color(0, 0, 255))  # Green wipe
"""
                
                """
                #-----Single wipe animation------
		for j in range(0,LED_COUNT):
                        singleWipe(strip, j, Color(255, 0, 0))
                for j in range(0,60):
                        singleWipe(strip, j, Color(0, 255, 0))
                for j in range(0,60):
                        singleWipe(strip, j, Color(0, 0, 255))
"""

                #-----Bunch wipe animation-----
                for j in range(0,LED_COUNT):
                        bunchWipe(strip, j, Color(8, 0, 0))
                for j in range(0,LED_COUNT):
                        bunchWipe(strip, j, Color(0, 8, 0))
                for j in range(0,LED_COUNT):
                        bunchWipe(strip, j, Color(0, 0, 8))
