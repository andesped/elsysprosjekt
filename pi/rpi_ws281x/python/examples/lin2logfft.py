import numpy as np
import soundfile
import time
from neopixel import *

# LED strip configuration:
LED_COUNT      = 60      # Number of LED pixels.
LED_PIN        = 12      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 60      # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)


N_FFT = 1000
F_MIN = 20
F_MAX = 22050
N_LEDS = LED_COUNT


def log2lin(f_lin, data_lin, f_log, data_log):
    if (len(f_lin) != len(data_lin)):
        print("Error: f_lin is not same length as data_lin")
        return
    if ((len(f_log) != N_LEDS) | (len(data_log) != N_LEDS)):
        print("Error: f_log or data_log is not correct length")
    i_log = 0
    i = 0
    while(i < len(f_lin)):
        if (f_lin[i] > f_log[i_log]):
                i_log += 1
                continue
        if(i_log >= len(f_log)):
            print("i_log out of bounds")
            return
        data_log[i_log] += data_lin[i]
        i += 1




def test():
    (data, SAMPLERATE) = soundfile.read('soundfiles/sine.wav');
    #print(data)
    fft_data = np.fft.rfft(data, n=N_FFT)
    fft_data = np.abs(fft_data)
    #print(len(fft_data))
    fft_freq = SAMPLERATE*np.fft.rfftfreq(N_FFT)
    #print(len(fft_freq))
    #f_lin = np.linspace(F_MIN, F_MAX, N_FFT)
    f_log = np.logspace(np.log10(F_MIN), np.log10(F_MAX), N_LEDS)
    #data_lin = [1]*len(f_lin)
    data_log = [0]*len(f_log)
    log2lin(fft_freq, fft_data, f_log, data_log)
    for i in range(N_LEDS):
        print(fft_freq[i], ': ', fft_data[i])
    for i in range(N_LEDS):
        print(f_log[i], ': ', data_log[i])
    print(data_log)
    return data_log


# Main program logic follows:
if __name__ == '__main__':
    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(0,0,0))
    strip.show()

    data = test()
    for i in range(len(data)):
        strip.setPixelColor(i,Color(int(data[i]),int(data[i]),int(data[i])))
    strip.show()

