import time
import os
import sys
import numpy as np
import sounddevice as sd
import multiprocessing as mp
from scipy import signal
import collections
import threading
from neopixel import *
import RPi.GPIO as GPIO
import serial

#Serial communication with arduino
arduino_serial = serial.Serial(port='/dev/ttyACM0', baudrate=9600)

#Sounddevice config
sd.default.device = "C-Media"
SAMPLERATE = sd.query_devices(sd.default.device, 'input')['default_samplerate']

#Number of microphone samples
NUM_SAMPLES = 2**10
#Total number of samples for FFT
N_FFT = 2**11
#Min and max frequency to show on led strip
F_MIN = 200
F_MAX = 20000

# LED strip configuration:
LED_COUNT      = 144     # Number of LED pixels.
LED_OFFSET     = 54      # Some leds at the beginning of the strip are not visible
LED_COUNT_IN_USE = LED_COUNT - LED_OFFSET
LED_PIN        = 12      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 200      # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert if using npn level converter

#Rotary encoder variables
clk = 5
dt = 6
counter = 41
n = LED_COUNT_IN_USE #cells in freqArray
def make_led_freq_array():
    freqs = np.empty(LED_COUNT_IN_USE, dtype=float)
    #The first 10 leds should have a distance of 10 hz. From there on it will be logspace
    n_first_leds = 10
    first_space = 30
    for i in range (n_first_leds):
        freqs[i] = i*first_space + F_MIN
    f_log_start = F_MIN + n_first_leds*first_space
    freqs[n_first_leds:] = np.logspace(np.log10(f_log_start),np.log10(F_MAX),
                                         LED_COUNT_IN_USE - n_first_leds)
    return freqs
#Each frequency is represented by 1 LED
#LED_FREQS = np.logspace(np.log10(F_MIN), np.log10(F_MAX), n)
LED_FREQS = make_led_freq_array()
#Make the first intervals a bit bigger
#LED_FREQS[0] = 90
#LED_FREQS[1] = 100
#Place frequencies in between each LED
freqArray = np.interp(np.arange(2*LED_COUNT_IN_USE-1),
                      np.arange(2*LED_COUNT_IN_USE-1,step=2), LED_FREQS)
N_FREQS = 2*LED_COUNT_IN_USE - 1
#mode variables
in1 = 4
in2 = 27
tinnitus_pin = 7
hearing_filter_pin = 8
MODE = mp.Value('i',3)
TINNITUS_STATE = mp.Value('b', True)
HEARING_LOSS_FILTER_STATE = mp.Value('b', True)
#SYSTEM_STATUS = mp.
#GPIO setups
GPIO.setmode(GPIO.BCM)
GPIO.setup(in1, GPIO.IN)
GPIO.setup(in2, GPIO.IN)
GPIO.setup(tinnitus_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(hearing_filter_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(clk, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(clk, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(dt, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
clkLastState = GPIO.input(clk) #define lastState for clk
#sinGen variables
FREQ = mp.Value('d', 440.0)

MANAGER = mp.Manager()
PROCESS_TIMESTAMPS = MANAGER.dict()


def filter_worker(samples_queue, filtered_samples_queue, fft_queue):
    global MODE
    global FREQ
    b, a = signal.butter(10, 4000/SAMPLERATE, btype='lowpass', analog=False) 
    zi = signal.lfilter_zi(b,a)
    n_next = 0
    n_tin_next = 0
    tinnitus_freq = LED_FREQS[50]
    sin_f_prev = 0
    sin_phase_next = 0
    tin_phase_next = 0
    scale_sin = 0.1
    scale_tin = 0.01
    scale_samples = 1

    while True:
        PROCESS_TIMESTAMPS[os.getpid()] = time.time() * 1000
        samples = scale_samples*samples_queue.get(block = True)
        if MODE.value == 1:
            sine, n_next, sin_phase_next = gen_sin(n_next,NUM_SAMPLES,
                                              FREQ.value/SAMPLERATE, sin_phase_next)
            samples[:] = scale_sin*sine
        if HEARING_LOSS_FILTER_STATE.value:
            filtered_samples, zi = signal.lfilter(b, a, samples, zi=zi)
            samples[:] = filtered_samples
        if TINNITUS_STATE.value:
            tinnitus, n_tin_next, tin_phase_next = gen_sin(n_tin_next,NUM_SAMPLES,tinnitus_freq/SAMPLERATE, tin_phase_next)
            #Make sure the tinnitus is not too loud
            samples[:] += scale_tin*tinnitus

        filtered_samples_queue.put(samples)
        fft_queue.put(samples)

circ_buf = mp.Array('f', N_FFT)
        
def fft_worker(ledstrip_queue):
    fft_freq = SAMPLERATE*np.fft.rfftfreq(N_FFT)
    led_f_intervals = find_led_f_intervals(freqArray)
    lin_f_indices = find_frequency_indices(led_f_intervals, fft_freq)
    while True:
        PROCESS_TIMESTAMPS[os.getpid()] = time.time() * 1000
        data = circ_buf[:]
        fft_data = np.fft.rfft(data)
        fft_data = simplify_fft(lin_f_indices, fft_data)
        ledstrip_queue.put(fft_data, block=True)

def fft_manager(fft_queue, ledstrip_queue):

    '''
    fft_process = mp.Process(target = fft_worker, args = (ledstrip_queue,))
    fft_process.start()
    '''

    #Record position in circular buffer
    pos = 0
    while True:
        PROCESS_TIMESTAMPS[os.getpid()] = time.time() * 1000
        data = fft_queue.get(block = True)
        circ_buf[pos:pos+NUM_SAMPLES] = data
        pos = (pos+NUM_SAMPLES) % N_FFT
    

def ledstrip_worker(ledstrip_queue):
    f_log = np.logspace(np.log10(F_MIN), np.log10(F_MAX), LED_COUNT_IN_USE)
    fft_freq = SAMPLERATE*np.fft.rfftfreq(N_FFT)
    led_f_intervals = find_led_f_intervals(freqArray)
    lin_f_indices = find_frequency_indices(led_f_intervals, fft_freq)
    data_log = np.empty(LED_COUNT_IN_USE, dtype=complex)
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
    strip.begin()
    scale = 0.10
    lower_bound = 100
    while True:
        PROCESS_TIMESTAMPS[os.getpid()] = time.time() * 1000
        data = ledstrip_queue.get(block = True)
        #data = simplify_fft(lin_f_indices, data)
        #print(max_f)
        #lin2log(fft_freq, data, f_log, data_log)
        '''
        data_log_re = data_log * np.conj(data_log)
        data_log_re = data_log_re * 1/10
        #data_log_re = np.abs(data_log)
        data_log_re = np.clip(data_log_re, 0, 255)
        '''
        data = scale*data
        data[:] = np.clip(data, 0, 255)
        data[data<lower_bound] = 0
        #Write loudest freq to arduino
        max_i = np.argmax(data)
        max_val = np.max(data)
        max_f = LED_FREQS[max_i]
        if (arduino_serial.in_waiting and max_val > 10):
            arduino_serial.write(bytes(str(int(max_f)),'ascii'))
            arduino_serial.write(b'\0')
            arduino_serial.reset_input_buffer()
        for i in range(LED_COUNT_IN_USE):
            val = int(data[i])
            strip.setPixelColor(LED_COUNT - 1 - i, Color(val,0,0))
        strip.show()

def polling_worker():
    global FREQ
    global clkLastState
    global counter
    global N_FREQS
    global MODE
    while True:
        PROCESS_TIMESTAMPS[os.getpid()] = time.time() * 1000
        time.sleep(0.000001)
        #Poll GPIO to select mode
        pin1 = GPIO.input(in1)
        pin2 = GPIO.input(in2)
        TINNITUS_STATE.value = GPIO.input(tinnitus_pin)
        HEARING_LOSS_FILTER_STATE.value = GPIO.input(hearing_filter_pin)
        #print(HEARING_LOSS_FILTER_STATE.value)
        if pin1==1 and pin2==0:
            MODE.value = 1
        if pin1==0 and pin2==0:
            MODE.value = 2
        if pin1==0 and pin2==1:
            MODE.value = 3	
        #print(MODE.value)

        #Sinus mode
        if MODE.value == 1:
            clkState = GPIO.input(clk)
            dtState = GPIO.input(dt)
            if clkState != clkLastState: #checks if turned
                #checks which way it's turned
                    if dtState != clkState:
                        if counter < N_FREQS-1:
                            counter += 1
                            FREQ.value = freqArray[counter]
                            print(FREQ.value)
                    else:
                        if counter > 0:
                            counter -= 1
                            FREQ.value = freqArray[counter]
                            print(FREQ.value)
            clkLastState = clkState

def find_led_f_intervals(led_freqs):
    led_f_intervals = np.empty(LED_COUNT_IN_USE, dtype=tuple)
    led_f_intervals[0] = (led_freqs[0], led_freqs[1])
    intervals_i = 1
    for i in range(2,len(led_freqs)-1,2):
        led_f_intervals[intervals_i] = (led_freqs[i-1], led_freqs[i+1])
        intervals_i = intervals_i + 1
    led_f_intervals[-1] = (led_freqs[-2],led_freqs[-1])
    return led_f_intervals


def find_frequency_indices(f_led_intervals, f_lin):
    lin_i = 0
    f_lin_indices = [list([0,0]) for _ in range(LED_COUNT_IN_USE)]
    for i in range(len(f_led_intervals)):
        interval = f_led_intervals[i]
        #Find indices of f_lin which are within interval
        f = f_lin[lin_i]
        #If f is too low, increase until it is within interval
        while f < interval[0]:
            lin_i = lin_i + 1
            f = f_lin[lin_i]
        #Add first index to list
        f_lin_indices[i][0] = lin_i
        while interval[0] <= f <= interval[1]:
            lin_i = lin_i + 1
            f = f_lin[lin_i]
        #Add last index to list
        f_lin_indices[i][1] = lin_i
    return f_lin_indices
        


def simplify_fft(f_lin_indices, f_data):
    data = np.zeros(LED_COUNT_IN_USE)
    #[a.real + a.imag for a in f_data]
    #f_data[:] = np.square(np.abs(f_data))
    #f_data[:] = np.abs(np.square(f_data))
    for i in range(len(f_lin_indices)):
        ind = f_lin_indices[i]
        '''
        j = ind[0]
        j_max = ind[1] + 1
        while j < j_max:
            val = f_data[j].real**2 + f_data[j].imag**2
            if val > data[i]:
                data[i] = val
        '''
        #data[i] = max(np.abs(f_data[ind[0]:(ind[1]+1)]))
        data[i] = max([a.real**2 + a.imag**2 for a in f_data[ind[0]:(ind[1]+1)]])
        #data[i] = np.max([a.real + a.imag for a in f_data[ind[0]:(ind[1]+1)]])
        #data[i] = max(f_data[ind[0]:(ind[1]+1)].real)
        #data[i] = max(np.abs(f_data[ind[0]:(ind[1]+1)]))
        #data[i] = 0
        #temp = f_data[ind[0]:(ind[1] + 1)]
        #data[i] = np.max(temp.real + temp.imag)
        #data[i] = np.max(np.abs(f_data[ind[0]:(ind[1]+1)]))
        #data[i] = np.max(f_data[ind[0]:(ind[1]+1)])

    return data



def lin2log(f_lin, data_lin, f_log, data_log):
    lin_len = len(f_lin)
    log_len = len(f_log)
    if (len(f_lin) != len(data_lin)):
        print("Error: f_lin is not same length as data_lin")
        return
    if ((len(f_log) != LED_COUNT_IN_USE) | (len(data_log) != LED_COUNT_IN_USE)):
        print("Error: f_log or data_log is not correct length")
        return
    f_step_lin = F_MAX/lin_len
    for i in range(log_len):
        index = int(round(f_log[i]/f_step_lin)) - 1
        data_log[i] = data_lin[index]

def gen_sin(n_start, num_samples, f, phase):
    n_end = n_start + num_samples
    #n = np.arange(n_start, n_end)
    n = np.arange(num_samples)
    signal = np.sin(2*np.pi*f*n + phase)
    phase_next = 2*np.pi*f*(num_samples) + phase
    return signal, n_end, phase_next


def microphone_worker(samples_queue, filtered_samples_queue):
    #microphone sampling
    scale_out = 1
    def in_callback(indata, outdata, frames, time, status):
        if any(indata):
            #outdata[:] = indata[:]
            #Send data to dsp process
            samples_queue.put(indata[:,0])
            #outdata[:,0] = indata[:,0]
            outdata[:,0] = scale_out*filtered_samples_queue.get(block = True)
            #filtered_samples_queue.get(block = True)

    with sd.Stream(device=sd.default.device, channels=1, callback=in_callback,
            blocksize=NUM_SAMPLES, dtype=np.float32,
            samplerate=SAMPLERATE):
        while True:
            PROCESS_TIMESTAMPS[os.getpid()] = time.time() * 1000
            time.sleep(0.001)
 

def main():
    #Sharing data between processes
    queue_size = 10
    samples_queue = mp.Queue(maxsize=queue_size)
    filtered_samples_queue = mp.Queue(maxsize=queue_size)
    fft_queue = mp.Queue(maxsize=queue_size)
    ledstrip_queue = mp.Queue(maxsize=queue_size)

    #Variable to hold processes
    processes = [] 

    #Start filtering process
    filter_process = mp.Process(target = filter_worker,
            args = (samples_queue, filtered_samples_queue, fft_queue),
            daemon = True, name = 'Filter process')
    processes.append(filter_process)
    #filter_process.start()

    #Start fft process
    fft_process1 = mp.Process(target = fft_manager, 
            args = (fft_queue,ledstrip_queue), daemon = True, name = 'FFT1 process')
    processes.append(fft_process1)
    #fft_process1.start()

    fft_process2 = mp.Process(target = fft_worker, args = (ledstrip_queue,),
            daemon = True, name = 'FFT2 process')
    processes.append(fft_process2)
    #fft_process2.start()

    #Start ledstrip process
    ledstrip_process = mp.Process(target = ledstrip_worker, 
            args = (ledstrip_queue,), daemon=True, name = 'Ledstrip process')
    processes.append(ledstrip_process)
    #ledstrip_process.start()

    #Start button polling worker
    polling_process = mp.Process(target = polling_worker, daemon = True,
            name = 'Polling process')
    processes.append(polling_process)
    #polling_process.start()

    #Process for sampling microphone
    mic_process = mp.Process(target = microphone_worker,
            args = (samples_queue, filtered_samples_queue), daemon = True,
            name = 'Mic process')
    processes.append(mic_process)
    #mic_process.start()

    for p in processes:
        p.start()
        print(p.name + ':' + str(p.pid))
    while True:
        sys.stdout.flush()
        time.sleep(1)
        print(PROCESS_TIMESTAMPS)
        for p in processes:
            if not p.is_alive():
                print('A process crashed')
                p.terminate()
                p.run()
                '''
                for p2 in processes:
                    p.terminate()
                for p2 in processes:
                    p2.start()
                '''

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.exit(0)

