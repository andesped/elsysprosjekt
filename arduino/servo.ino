Servo myservo;  // create servo object to control a servo
long freq; //Frequency between 20 and 20 000
int pos = 0; // variable to store the servo position
int low = 20; //lowest position for the servomotor
int high = 50; //highest position for the servomotor

void setup() {
  myservo.attach(9); 
  Serial.begin(9600);
}
void loop() {
  Serial.write(1);
  freq = Serial.parseInt();
  if (freq>0){
   if (freq<200){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);            
      delay(15);                  
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);             
      delay(15);                  
    }
  }
    else if (freq<400){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);             
      delay(14);                     
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);            
      delay(14);                    
    }
  }
  else if (freq<550){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);             
      delay(13);                      
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);             
      delay(13);                       
    }
  }
    else if (freq<900){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);             
      delay(12);                  
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);             
      delay(12);                    
    }
  }
    else if (freq<1300){
      for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);             
      delay(11);                      
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);              
      delay(11);                   
    }
  }
    else if (freq<1800){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);         
      delay(10);                      
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);          
      delay(10);                     
    }
  }
    else if (freq<4600){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);           
      delay(9);                       
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);              
      delay(9);                      
    }
  }
    else if (freq<6250){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);              
      delay(8);                      
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);              
      delay(8);                      
    }
  }
     else if (freq<8000){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);             
      delay(7);                       
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);              
      delay(7);                      
    }
  }
  else if (freq<10500){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);              
      delay(6);                      
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);             
      delay(6);                      
    }
  }
  else if (freq>=10500){
    for (pos = low; pos <= high; pos += 1) { 
      // in steps of 1 degree 
      myservo.write(pos);              
      delay(5);                      
    }
    for (pos = high; pos >= low; pos -= 1) { 
      myservo.write(pos);             
      delay(5);                     
    }
  }
  }
}


